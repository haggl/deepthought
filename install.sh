#!/bin/sh
TARGET=$HOME/.local/bin/deepthought

# Assemble Haskell module list
MODULES="`ls src | sed 's:\.hs::' | tr "\n" ' '`"

# Perform substitutions on deepthought script template
SUBSTITUTIONS="$SUBSTITUTIONS; s:__var_root__:$PWD:"
SUBSTITUTIONS="$SUBSTITUTIONS; s:__var_modules__:$MODULES:"
sed "$SUBSTITUTIONS" bin/deepthought > $TARGET
chmod +x $TARGET
