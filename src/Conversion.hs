module Conversion
       ( intToBin
       , binToInt
       , intToHex
       , hexToInt
       , floatToHex
       , hexToFloat
       , intToFixed
       , fixedToInt
       , floatToFixed
       , fixedToFloat
       , frac
       , uintToSint
       , sintToUint
       , hexToStr
       , strToHex
       , inchToCm
       , cmToInch
       ) where

import Data.Binary.IEEE754 (floatToWord, wordToFloat)
import Data.Bits (complement, isSigned)
import Data.Char (chr, digitToInt, intToDigit, ord)
import Data.List.Split (chunksOf)
import Numeric (showHex, showIntAtBase)


--
-- Internal helper functions
--

padZeros :: Int -> String -> String
padZeros blocksize digits | length digits `mod` blocksize == 0  = digits
                          | otherwise                           = padZeros blocksize $ "0" ++ digits

formatBin :: String -> String
formatBin binstring = (++) "0b" $ padZeros 8 binstring

formatHex :: String -> String
formatHex hexstring = (++) "0x" $ padZeros 4 hexstring


--
-- Binary conversions
--

intToBin :: Integer -> String
intToBin value = showIntAtBase 2 intToDigit value ""

binToInt :: String -> Integer
binToInt binstring | length binstring /= 0 && last binstring == '1' = 2 * binToInt (init binstring) + 1
                   | length binstring /= 0 && last binstring == '0' = 2 * binToInt (init binstring)
                   | length binstring == 0                          = 0
                   | otherwise                                      = undefined


--
-- Hexadecimal conversions
--

intToHex :: (Integral a, Show a) => a -> String
intToHex value | (-2^31) <= value && value < (-2^15) = intToHex $ 2^32 - abs value
               | (-2^15) <= value && value < (-2^ 7) = intToHex $ 2^16 - abs value
               | (-2^ 7) <= value && value < 0       = intToHex $ 2^ 8 - abs value
               | 0 <= value                          = showHex value ""
               | otherwise                           = "Error: unsupported operand!"

hexToInt :: String -> Integer
hexToInt hexstring | length hexstring /= 0 = 16 * hexToInt(init(hexstring)) + fromIntegral(digitToInt(last(hexstring)))
                   | otherwise             = 0

floatToHex :: Float -> String
floatToHex value = intToHex $ floatToWord value

hexToFloat :: String -> Float
hexToFloat hexstring = wordToFloat $ fromInteger $ hexToInt hexstring


--
-- Fixed point conversions
--

intToFixed :: Int -> Integer -> Integer
intToFixed precision value = value * 2^precision

fixedToInt :: Int -> Integer -> Integer
fixedToInt precision value = floor $ fromInteger value / 2^precision

floatToFixed :: Int -> Float -> Integer
floatToFixed precision value = floor $ value * 2^precision

fixedToFloat :: Int -> Integer -> Float
fixedToFloat precision value = fromInteger value / 2^precision

-- | Fractional function (unsigned!)
frac :: RealFrac a => a -> a
frac = abs . snd . properFraction


--
-- Two's complement conversions
--

uintToSint :: Int -> Integer -> Integer
uintToSint resolution value | value < 2^(resolution-1) = value
                            | otherwise                = value - 2^resolution

sintToUint :: Int -> Integer -> Integer
sintToUint resolution value | isSigned value = value + 2^resolution
                            | otherwise      = value


--
-- String conversions
--

strToHex :: String -> String
strToHex s = concat $ map (intToHex . ord) s

hexToStr :: String -> String
hexToStr s = map (chr . fromIntegral . hexToInt) $ chunksOf 2 s



---
--- Unit conversions
---

inchToCm :: Float -> Float
inchToCm n = 2.51 * n

cmToInch :: Float -> Float
cmToInch n = n / 2.51
