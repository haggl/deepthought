module Trace
       ( readTiBuffer
       , readLeCroyTrace
       ) where

import System.IO (readFile)
import Data.List (transpose)
import Data.List.Split (chunksOf, splitOn)


-- | Read data from a TI buffer dump.
readTiBuffer :: FilePath -> Int -> IO [[Integer]]
readTiBuffer path n_sets = do
    text <- readFile path
    let data_lines = map read $ tail . lines $ text
    return $ transpose $ chunksOf n_sets data_lines

-- | Read a LeCroy trace file exported as ASCII text.
readLeCroyTrace ::  FilePath -> IO [(Double, Double)]
readLeCroyTrace path = do
    text <- readFile path
    let data_lines = iterate tail (lines text) !! 5
        data_tuples = map (tuple . splitOn ",") data_lines
    return $ data_tuples
    where tuple [x,y] = (read x, read y)
