module Level
       ( ld
       , lg
       , ln
       , rmsToDBV
       , dBVToRMS
       , vToDBV
       , dBVToV
       ) where

-- | Binary logarithm
ld :: Floating a => a -> a
ld x = log x / log 2

-- | Decimal logarithm
lg :: Floating a => a -> a
lg x = log x / log 10

-- | Natural logarithm
ln :: Floating a => a -> a
ln = log

-- | Conversion from V (RMS) to dBV
rmsToDBV :: Floating a => a -> a
rmsToDBV x = 20 * lg x

-- | Conversion from dBV to V (RMS)
dBVToRMS :: Floating a => a -> a
dBVToRMS x = 10**(x/20)

-- | Conversion from peak voltage to dBV
vToDBV :: Floating a => a -> a
vToDBV x = rmsToDBV $ x / 2**0.5

-- | Conversion from dBV to peak voltage
dBVToV :: Floating a => a -> a
dBVToV x = 2**0.5 * dBVToRMS x
