module Plot
       ( PlotData
       , PlotMethod(Interactive,Standalone)
       , PlotType(Dots,Lines,LinesPoints,Points)
       , grid
       , mkPlotData
       , plot2D
       , plot2D'
       , preparePlot2D
       , select
       , plotSignal
       , plotSignals
       , title
       , xLabel
       , xLogScale
       , xRange2d
       , yLabel
       , yLogScale
       , yRange2d
       ) where

import Prelude hiding (lines)

import Control.Monad (void)
import Data.List.Split (chunksOf)
import Graphics.Gnuplot.Advanced (plotAsync, plotSync)
import Graphics.Gnuplot.Frame.OptionSet (grid, title, xLabel, xLogScale, xRange2d, yLabel, yLogScale, yRange2d)
import Graphics.Gnuplot.Graph.TwoDimensional (dots, lineSpec, lines, linesPoints, points)
import Graphics.Gnuplot.Plot.TwoDimensional (list)
import Graphics.Gnuplot.Simple (PlotType(Dots,Lines,LinesPoints,Points))
import System.Environment (getProgName)

import qualified Graphics.Gnuplot.Frame as Frame (T, cons)
import qualified Graphics.Gnuplot.Frame.OptionSet as FrameOpts (T, deflt)
import qualified Graphics.Gnuplot.Graph as Graph (C)
import qualified Graphics.Gnuplot.Graph.TwoDimensional as Graph2D (T)
import qualified Graphics.Gnuplot.LineSpecification as LineSpec (deflt, title)
import qualified Graphics.Gnuplot.Plot.TwoDimensional as Plot2D (T)
import qualified Graphics.Gnuplot.Terminal.X11 as X11 (cons)

data PlotMethod = Interactive -- ^ Asynchronous X11 plot (in GHCi)
                | Standalone  -- ^ Synchronous X11 plot (in compiled programs)

-- | Type synonym for labeled (x,y) data.
type PlotData = ([(Double, Double)], String)

-- | Type synonym for 2D plot frame options
type FrameOpts2D = FrameOpts.T (Graph2D.T Double Double)

-- | Constructs PlotData from x- and y-vectors and a label.
mkPlotData :: String    -- ^ Label for the plot
           -> [Double]  -- ^ x-coordinate vector
           -> [Double]  -- ^ y-coordinate vector
           -> PlotData
mkPlotData label xs ys = (zipWith tuple xs ys, label)
  where tuple x y = (x,y)

-- | Plots a list of 2D plots in a single window.
plot2D :: (FrameOpts2D -> FrameOpts2D) -- ^ Plot settings
       -> [Plot2D.T Double Double]     -- ^ Plots to combine in one window
       -> IO ()
plot2D settings ps = do
    progName <- getProgName
    if progName == "<interactive>" then
        plot2D' Interactive settings ps
    else
        plot2D' Standalone  settings ps

-- | Plots a list of 2D plots in a single window.
plot2D' :: PlotMethod                   -- ^ Used plotting strategy
        -> (FrameOpts2D -> FrameOpts2D) -- ^ Plot settings
        -> [Plot2D.T Double Double]     -- ^ Plots to combine in one window
        -> IO ()
plot2D' method settings ps = case method of
    Standalone -> plot plotSync settings ps
    _          -> plot plotAsync settings ps
  where plot m ss ps = void $ m X11.cons $ preparePlotFrame2D ss ps

-- | Quickly plots a data vector
plotSignal :: String    -- ^ Label for plotted data
           -> [Double]  -- ^ Data to plot
           -> IO ()
plotSignal label ys = plot2D (grid True) $ [prepareVectorPlot label ys]

-- | Quickly plots a data vector
plotSignals :: [(String, [Double])]  -- ^ List of labels and signals to plot
            -> IO ()
plotSignals ss = plot2D (grid True) $ map (uncurry prepareVectorPlot) ss

-- | Construct a 2D points plot from labeled point vector.
preparePlot2D :: PlotType               -- ^ Line style to use for plotting
              -> PlotData               -- ^ Labeled (x,y) data
              -> Plot2D.T Double Double -- ^ Resulting 2D plot
preparePlot2D style (ps, label) = case style of
    Dots        -> prepare dots        label ps
    Lines       -> prepare lines       label ps
    LinesPoints -> prepare linesPoints label ps
    _           -> prepare points      label ps
  where lineStyle str      = lineSpec $ LineSpec.title str LineSpec.deflt
        prepare sty str ps = fmap (lineStyle str) $ list sty ps

-- | Prepare a 2D plot frame.
preparePlotFrame2D :: (FrameOpts2D -> FrameOpts2D)      -- ^ Plot settings
                   -> [Plot2D.T Double Double]          -- ^ Plots to combine in one window
                   -> Frame.T (Graph2D.T Double Double) -- ^ Resulting 2D plot frame
preparePlotFrame2D settings ps = Frame.cons (settings FrameOpts.deflt) $ mconcat ps

-- | Prepare a 2D plot of a signal.
prepareVectorPlot :: String                 -- ^ Label for plotted data
                  -> [Double]               -- ^ Signal to plot
                  -> Plot2D.T Double Double -- ^ Resulting 2D plot
prepareVectorPlot label ys = let xend  = fromIntegral $ length ys - 1
                                 pdat  = mkPlotData label [0..xend] ys
                             in preparePlot2D Lines pdat

-- | Select a range of values from a data set.
select :: Int -- ^ Maximum length of result vector
       -> Int -- ^ Offset
       -> Int -- ^ Consider only every nth input data point
       -> [a] -- ^ Input vector
       -> [a] -- ^ Result vector
select n o d v = take n $ drop o $ (map head . chunksOf d) v
