module Signal
       ( doubleSamplerate
       , powerSpectrum
       , signalArray
       ) where

import DSP.Filter.FIR.FIR
import Data.Array (Array, elems, listArray)
import Data.Complex (Complex((:+)))
import Numeric.Transform.Fourier.FFTUtils (fft_db)

-- | Converts a signal to an Array
signalArray :: [t]          -- Signal vector
            -> Array Int t
signalArray s = listArray (0, length s - 1) s

-- | Double the samplerate of a signal by inserting zeros and LP-filtering.
doubleSamplerate :: [Double] -- ^ low-pass filter coefficients
                 -> [Double] -- ^ signal vector
                 -> [Double] -- ^ up-sampled signal
doubleSamplerate h s = drop padLength $ fir (signalArray h) $ prepare s
  where padLength     = length h
        insertZeros s = concat $ map (\x -> x:0:[]) s
        periodicPad s = drop (length s - padLength) s ++ s
        --periodicPad s = s ++ take padLength s
        prepare     s = periodicPad $ insertZeros s

-- | Computes the FFT magnitude of a signal
powerSpectrum :: [Double] -- Signal vector
              -> [Double] -- Frequency magnitude spectrum
powerSpectrum s = take (length spectrum `div` 2) spectrum
  where spectrum = elems $ fft_db $ signalArray $ map (:+ 0) s
