module Currency
       ( fetchExchangeRate
       , fromCurrency
       , fromDEM
       , fromGBP
       , fromUSD
       , toDEM
       , toGBP
       , toUSD
       , toCurrency
       ) where

import Data.Maybe (fromJust)
import GHC.Generics
import Network.HTTP.Simple (Response, getResponseBody, httpLBS, parseRequest)
import Text.XML.Light.Input (parseXML)
import Text.XML.Light.Proc (findAttr, elChildren, findChildren, onlyElems)
import Text.XML.Light.Types (QName(QName))

fetchExchangeRate :: String -> IO Float
fetchExchangeRate currency = do
    request <- parseRequest "https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml"
    response <- httpLBS request
    let elements    = onlyElems $ parseXML $ getResponseBody response
        selector    = QName "Cube" (Just "http://www.ecb.int/vocabulary/2002-08-01/eurofxref") Nothing
        container   = head $ concatMap (findChildren selector) elements
        currencies  = elChildren $ head $ elChildren container
        lookupTable = [(getAttr "currency" c, getAttr "rate" c) | c <- currencies]
    return $ read $ fromJust $ lookup currency lookupTable
    where getAttr n = fromJust . findAttr (QName n Nothing Nothing)

-- | Converts from specified currency to EUR
fromCurrency :: String -> Float -> IO Float
fromCurrency currency value = do
    exchangeRate <- fetchExchangeRate currency
    return $ value / exchangeRate

-- | Converts from DEM to EUR
fromDEM :: Float -> Float
fromDEM = (/ toDEM 1)

-- | Converts from GBP to EUR
fromGBP :: Float -> IO Float
fromGBP = fromCurrency "GBP"

-- | Converts from USD to EUR
fromUSD :: Float -> IO Float
fromUSD = fromCurrency "USD"

-- | Converts from EUR to specified currency
toCurrency :: String -> Float -> IO Float
toCurrency currency value = do
    exchangeRate <- fetchExchangeRate currency
    return $ value * exchangeRate

-- | Converts from EUR to DEM
toDEM :: Float -> Float
toDEM = (* 1.95583)

-- | Converts from EUR to GBP
toGBP :: Float -> IO Float
toGBP = toCurrency "GBP"

-- | Converts from EUR to USD
toUSD :: Float -> IO Float
toUSD = toCurrency "USD"
